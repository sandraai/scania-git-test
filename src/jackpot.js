
function testJackpot_for (array) {
    
    for (i = 0; i < array.length-1; i++) {
    
        if (array[i] !== array[i+1]) {
            return false
        }
    }
    return true
}



function testJackpot_every (array) {
    const firstValue = array[0]
    const isEqual = (currentValue) => currentValue === firstValue
    const result = array.every(isEqual)
    return result
}


console.log(testJackpot_for(['1', '1', '1', '1']))
console.log(testJackpot_for(['abc', 'abd', 'abc', 'abc']))
console.log(testJackpot_every(['1', '1', '1', '2']))
console.log(testJackpot_every(['o', 'o', 'o', 'o']))


