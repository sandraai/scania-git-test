// FizzBuzz


/*
function printFizzBuzz (numberSequence) {
    let message = ''
    for(i = 1; i <= numberSequence; i++){
        if (i % 3 == 0 && i % 5 == 0){
            message = 'FizzBuzz'
        } else if (i % 3 == 0) {
            message = 'Fizz'
        } else if (i % 5 == 0) {
            message = 'Buzz'
        } else {
            message = i
        }
        console.log(message)
    }
}

printFizzBuzz(100)
*/

function printFizzBuzz (number) {
    let message = ''
    if (number % 3 == 0 && number % 5 == 0){
        message = 'FizzBuzz'
    } else if (number % 3 == 0) {
        message = 'Fizz'
    } else if (number % 5 == 0) {
        message = 'Buzz'
    } else {
        message = number
    }
    console.log(message)
}

function printSequence (numberSequence) {
    for(i = 1; i <= numberSequence; i++) {
        printFizzBuzz(i)
    }
}
//printFizzBuzz(15)
printSequence(100)

