
function replaceVowel(string) {
    const lowerCaseString = string.toLowerCase()
    let newString = ''
    for (let i = 0; i < lowerCaseString.length; i++) {
        switch (lowerCaseString[i]) {
            case 'a':
                newString += 1
                break
            case 'e':
                newString += 2
                break
            case 'i':
                newString += 3
                break
            case 'o':
                newString += 4
                break
            case 'u':
                newString += 5
                break
            default:
                newString += lowerCaseString[i]
        }
    }
    return newString
}


function replaceVowelShort(string) {
    let lowerCaseString = string.toLowerCase()
    let newString = lowerCaseString.replace(/a/g, '1').replace(/e/g, '2').replace(/i/g, '3').replace(/o/g, '4').replace(/u/g, '5')
    return newString
}

console.log(replaceVowel('karAchi'))
console.log(replaceVowel('chEmBur'))
console.log(replaceVowel('khandbari'))
console.log(replaceVowel('LexiCAl'))
console.log(replaceVowel('fuNctionS'))
console.log(replaceVowel('EASY'))

console.log(replaceVowelShort('karAchi'))
console.log(replaceVowelShort('chEmBur'))
console.log(replaceVowelShort('khandbari'))
console.log(replaceVowelShort('LexiCAl'))
console.log(replaceVowelShort('fuNctionS'))
console.log(replaceVowelShort('EASY'))