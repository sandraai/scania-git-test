

function isPalindrome (string) {
    const lowerCaseString = string.toLowerCase()
    let reversedString = ''
    for (let i = lowerCaseString.length-1; i >= 0; i--) {
        reversedString += lowerCaseString[i]
    }
    if (reversedString === lowerCaseString) {
        return true
    }
    return false
}

console.log(isPalindrome('m_7_m'))
console.log(isPalindrome('Madam'))
console.log(isPalindrome('abba'))
console.log(isPalindrome('apple'))
console.log(isPalindrome('bravo'))


function isPalindrome2 (string) {
    const lowerCaseString = string.toLowerCase()
    const reversedString = lowerCaseString.split('').reverse().join('')
    
    if (reversedString === lowerCaseString) {
        return true
    }
    return false
}

console.log(isPalindrome2('m_7_m'))
console.log(isPalindrome2('Madam'))
console.log(isPalindrome2('abba'))
console.log(isPalindrome2('apple'))
console.log(isPalindrome2('bravo'))