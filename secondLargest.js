

function secondLargest (array) {
    if (array.length === 0 || (array.length === 2 && array[0] === array[1])) {
        return 0
    } else if (array.length === 1) {
        return array[0]
    } else {
        array.sort( (a, b) => a - b )
        let maxValue = array[array.length-1]
        let result = 0
        for (let i = array.length-2; i >= 0; i--) {
            if (array[i] < maxValue) {
                result = i
                break
            }
        }
        return array[result]
    }
}

console.log(secondLargest([10, 40, 30, 20, 50]))
console.log(secondLargest([25, 143, 89, 13, 105]))
console.log(secondLargest([25, 143, 89, 13, 105, 105, 143, 143]))
console.log(secondLargest([54, 23, 11, 17, 10]))
console.log(secondLargest([1, 1]))
console.log(secondLargest([1]))
console.log(secondLargest([]))