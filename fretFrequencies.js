const stringFreqs = [
    {stringNo: 6, freq: '82.41'},
    {stringNo: 5, freq: '110.00'},
    {stringNo: 4, freq: '146.83'},
    {stringNo: 3, freq: '196.00'},
    {stringNo: 2, freq: '246.94'},
    {stringNo: 1, freq: '329.63'}]

function fretFreqs (stringNumber, fret) {
    const stringObj = stringFreqs.find(element => element.stringNo === stringNumber)
    
    return (stringObj.freq * 2**(fret/12)).toFixed(2)
}

// Kan också lösas med object map, och slå upp med t.ex. stringFreqs[6], blir 82.41

console.log(fretFreqs(5, 12))
console.log(fretFreqs(4, 6))
console.log(fretFreqs(2, 23))